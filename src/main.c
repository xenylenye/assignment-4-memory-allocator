#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region *r);
static inline void *block_after(struct block_header const *block);

static inline bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}
static inline size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static inline size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static inline size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

static void block_init(void *restrict addr, block_size block_sz,
                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static void *map_pages_with_fallback(void const *addr, size_t size, int flags) {
  void *mapped_addr = map_pages(addr, size, flags);
  if (mapped_addr == MAP_FAILED) {
    // Try without MAP_FIXED_NOREPLACE
    mapped_addr = map_pages(addr, size, 0);
  }
  return mapped_addr;
}

static inline void initialize_block_if_valid(void *region_addr, size_t size) {
  if (region_addr != MAP_FAILED) {
    block_init(region_addr, (block_size){.bytes = size}, NULL);
  }
}

static inline struct region create_region(void *addr, size_t size, bool extends) {
  return (struct region){.addr = addr, .size = size, .extends = extends};
}

static struct region alloc_region(void const *addr, size_t query) {
  size_t actual_size = region_actual_size(
      size_from_capacity((block_capacity){.bytes = query}).bytes);
  void *region_addr =
      map_pages_with_fallback(addr, actual_size, MAP_FIXED_NOREPLACE);
  initialize_block_if_valid(region_addr, actual_size);

  if (region_addr == MAP_FAILED) {
    return REGION_INVALID;
  }

  return create_region(region_addr, actual_size, region_addr == addr);
}


void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  bool isSplittable = (block != NULL) && block_splittable(block, query);
  bool splitSuccess = false;

  if (isSplittable) {
    size_t adjustedQuery = size_max(query, BLOCK_MIN_CAPACITY);
    block_size newBlockSize = {.bytes = block->capacity.bytes - adjustedQuery};
    void *splitBlockAddr = (char *)block->contents + adjustedQuery;

    block_init(splitBlockAddr, newBlockSize, block->next);

    block->next = splitBlockAddr;
    block->capacity.bytes = adjustedQuery;

    splitSuccess = true;
  }

  return splitSuccess;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
  struct block_header *next = block->next;
  if (block != NULL && next != NULL) {
    if (mergeable(block, next)) {
      block->capacity.bytes += size_from_capacity(next->capacity).bytes;
      block->next = next->next;
      return true;
    }
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static bool is_suitable_block(struct block_header *block, size_t sz) {
  if (!block->is_free)
    return false;
  while (try_merge_with_next(block))
    ;
  return block_is_big_enough(sz, block);
}

static struct block_search_result create_block_search_result(int type, struct block_header *block) {
  struct block_search_result result;
  result.type = type;
  result.block = block;
  return result;
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
  for (; block; block = block->next) {
    if (is_suitable_block(block, sz)) {
      return create_block_search_result(BSR_FOUND_GOOD_BLOCK, block);
    }
    if (!block->next) {
      return create_block_search_result(BSR_REACHED_END_NOT_FOUND, block);
    }
  }
  // In case the loop never executes:
  return create_block_search_result(BSR_CORRUPTED, NULL);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return result;
  }
  return result;
}

static struct block_header *create_block(struct block_header *last_block,
                                         void *region_addr,
                                         bool extends) {
  last_block->next = region_addr;

  struct block_header *new_block =
      (extends && try_merge_with_next(last_block))
          ? last_block
          : last_block->next;

  return new_block;
}

static struct block_header *grow_heap(struct block_header *restrict last_block,
                                      size_t size_request) {
  if (last_block == NULL) {
    return NULL;
  }

  void *requested_region_start = block_after(last_block);
  struct region new_memory_region =
      alloc_region(requested_region_start, size_request);

  if (region_is_invalid(&new_memory_region)) {
    return NULL;
  }

  struct block_header *new_block = create_block(last_block, new_memory_region.addr,
                                                new_memory_region.extends);

  return new_block;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  if (!heap_start)
    return NULL;

  query = size_max(query, BLOCK_MIN_CAPACITY);

  struct block_search_result result = try_memalloc_existing(query, heap_start);
  switch (result.type) {
  case BSR_FOUND_GOOD_BLOCK:
    return result.block;
  case BSR_CORRUPTED:
    return NULL;
  default:
    break;
  }

  struct block_header *expanded = grow_heap(result.block, query);
  return expanded ? try_memalloc_existing(query, heap_start).block : NULL;
}

void *_malloc(size_t query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}
/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header *current = HEAP_START;
    struct block_header *next;

    if (!current) return;

    do {
        for (next = current->next; next && blocks_continuous(current, next); next = next->next) {
            current->capacity.bytes += size_from_capacity(next->capacity).bytes;
        }
        size_t current_size = size_from_capacity(current->capacity).bytes;
        munmap(current, current_size);
        current = next;
    } while (current);
}

void _free(void *mem) {
  if (mem == NULL)
    return;

  struct block_header *header = block_get_header(mem);
  header->is_free = true;

  while (try_merge_with_next(header))
    ; // Intentionally empty;
}
